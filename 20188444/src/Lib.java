public static void show(){
        Set<Map.Entry<String, Integer>> entries = wordCount.entrySet();// 排序

        ArrayList<String> words = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : entries) {
            words.add(entry.getValue()+"#"+entry.getKey());
        }


        Collections.sort(words);         // 排序
        words.forEach(obj->{
            String[] split = obj.split("#");
            String str = split[1]+": "+split[0];
            System.out.println(str);
        });
    }

    public  static void doCheck(File inputFile) throws Exception{
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(inputFile)));//使用了缓冲，提高输入输出效率
        String line = null;
        while(null!=(line=br.readLine())){//readLine()方法能一次从流中读入一行
            incrLine();   // 分析每一行。

            analysis(line);
        }
    }


    public static void incrLine(){        // 行数叠加
       count++;
      
    }


    public static long obtainTotalWords(){         //总单词数
        long sum = 0;
        Set<Map.Entry<String, Integer>> entries = wordCount.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            sum+=entry.getValue();
        }
        return sum;
    }


    public static void analysis(String line){       
        String [] words = line.split(" ");
        for(int i=0;i<words.length;i++){
            String word = words[i].trim();
            word = word.toLowerCase();
            word = word.contains(",")||word.contains(".")?word.substring(0,word.length()-1):word;
            if(word.length()>=4&&isWord(word.substring(0,4))){
                if(wordCount.containsKey(word)){
                    Integer count = wordCount.get(word);
                    count++;
                    wordCount.put(word,count);
                }else{
                    wordCount.put(word,1);
                }
            }
        }
    }

    public static boolean isWord(String word){
        for(int i=0;i<word.length();i++){
            if(word.charAt(i)>=97 && word.charAt(i)<=122){
                continue;
            }else{
                return false;
            }
        }
        return true;
        
    }